import re
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from django.http import JsonResponse
import json



# Create your views here.
# @require_http_methods(["GET", "POST"])
# def api_list_hats(request):
#     if request.method == "GET":
#         hats = Hat.objects.all()
#         return JsonResponse(
#             {"hats": hats},
            
#         )
#     else:
#         content = json.loads(request.body)
#         try:
#             location = Location.objects.get(id=location_id)
#             content["location"] = location 
#         except Location.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid location id"},
#                 status=400,
                
#             )
#         hat = Hat.create(**content)
#         return JsonResponse(
#             hat,
#             safe=False,
#         )
        
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is None:
            hats = Hat.objects.all()
            return JsonResponse(
                {"hats": hats},
            )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["hat"]
            location = LocationVO.objects.get(import_href=location_href)
            content["hat"] = Hat
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
            )
        shoe = Hat.objects.create(**content)
        return JsonResponse(
            shoe,
            safe=False,
        )
            